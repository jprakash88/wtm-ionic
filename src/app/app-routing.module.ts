import { CustommemeComponent } from './pages/examples/custommeme/custommeme.component';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { IndexComponent } from "./pages/index/index.component";
import { LandingpageComponent } from "./pages/examples/landingpage/landingpage.component";
import { ContactComponent } from './pages/examples/contact/contact.component';
import { AboutComponent } from './pages/examples/about/about.component';

const routes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: IndexComponent },
  { path: "meme/:id", component: LandingpageComponent },
  { path: "custommeme/:name", component: CustommemeComponent },
  { path: "contact", component: ContactComponent },
  { path: "about", component: AboutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

