import { Meme } from "src/app/Templates";
import { TemplateService } from "./../../../template.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: "app-landingpage",
  templateUrl: "landingpage.component.html"
})
export class LandingpageComponent implements OnInit, OnDestroy {
  meme: Meme = { id: "", name: "", box_count: 0, height: 0, url: "", width: 0 };
  memes: any = [];
  isCollapsed = true;
  memeForm: FormGroup;
  menuStatus: Boolean = false;
  imgUrl: string;
  imgName: string;
  loaderStatus: Boolean = false;
  btnStatus: Boolean = false;
  boxCount: Number = 0;
  boxCountArr: any = [];
  box0: any = "";
  box1: any = "";
  box2: any = "";
  box3: any = "";
  box4: any = "";
  boxVarArr: any = [];
  imgLoader: Boolean = false;

  constructor(
    private route: ActivatedRoute,
    private templateService: TemplateService,
    private formBuilder: FormBuilder,
    private androidPermissions: AndroidPermissions,
    private transfer: FileTransfer,
    private file: File,
    private photoViewer: PhotoViewer
  ) { }


  ngOnInit() {

    this.boxVarArr = [];
    this.boxCountArr = [];
    this.imgLoader = true;

    this.route.paramMap.subscribe(params => {
      this.templateService.fetchTemplates().subscribe(temp => {
        this.memes = temp.data.memes;
        this.templateService.templates = this.memes;
        this.meme = this.templateService.templates.find(
          meme => meme.id === params.get("id")
        );
        this.boxCount = this.meme.box_count;
        for (let i = 0; i < this.boxCount; i++) {
          this.boxCountArr.push(i);
          this.boxVarArr.push("");
        }
        this.imgLoader = false;

      });
    });

    const body = document.getElementsByTagName("body")[0];
    body.classList.add("landing-page");
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("landing-page");
  }

  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
        this.download(this.imgName, this.imgUrl);

      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
    this.androidPermissions.requestPermissions(
      [
        this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ]
    );
  }
  download(name, imgUrl) {
    const url = imgUrl;
    const fileTransfer: FileTransferObject = this.transfer.create();
    let fileName = name;
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {

        if (result.hasPermission) {
          fileTransfer.download(url, this.file.externalRootDirectory + "/Download/" + fileName).then((entry) => {

            this.photoViewer.show(entry.toURL(), fileName, { share: true });
            this.loaderStatus = false;
            this.btnStatus = false;
            this.boxVarArr = [];
            alert('Download complete: ' + entry.toURL());
          }, (error) => {
            this.checkPermissions();
          });
        } else {
          this.androidPermissions.requestPermissions(
            [
              this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
              this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
            ]

          );
          this.download(this.imgName, this.imgUrl);
        }

      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
  }


  onSubmit() {
    this.imgName = "";
    this.imgUrl = "";
    this.loaderStatus = true;
    this.btnStatus = true;
    var boxesArr = [];
    for (let j = 0; j < this.boxVarArr.length; j++) {

      let textItem = {
        "text": this.boxVarArr[j],
      }
      boxesArr.push(textItem);
    }

    this.templateService
      .generateMeme(this.meme.id, boxesArr)
      .subscribe((res) => {
        if (res.success === true) {
          var link = document.createElement('a');
          link.href = res.data.url;
          link.download = this.meme.name + '.jpg';
          this.imgUrl = res.data.url;
          this.imgName = this.meme.name + '.jpg';
          this.download(this.imgName, this.imgUrl);
          // document.body.appendChild(link);
          // link.click();
          // document.body.removeChild(link);
          // window.open(res.data.url, "_blank");
        }
      });
  }

  openNav() {
    if (this.menuStatus) {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("menu-elements").style.display = "none";
      this.menuStatus = !this.menuStatus;
    } else {
      document.getElementById("mySidenav").style.width = "50%";
      document.getElementById("menu-elements").style.display = "block";
      this.menuStatus = !this.menuStatus;
    }
  }
}
