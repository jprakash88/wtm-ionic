import { Component, OnInit } from "@angular/core";
import { Meme } from "src/app/Templates";
import { FormGroup, FormBuilder, FormControl } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { TemplateService } from "src/app/template.service";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { Platform } from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: "app-custommeme",
  templateUrl: "./custommeme.component.html",
  styleUrls: ["./custommeme.component.scss"]
})
export class CustommemeComponent implements OnInit {
  meme: Meme = { id: "", name: "", box_count: 0, height: 0, url: "", width: 0 };
  name: string = "";
  memes: Meme[] = [];
  isCollapsed = true;
  memeForm: FormGroup;
  loaderStatus: Boolean = false;
  btnStatus: Boolean = false;
  menuStatus: Boolean = false;
  blobObj: any;
  imgName: string;

  constructor(
    private route: ActivatedRoute,
    private templateService: TemplateService,
    private formBuilder: FormBuilder,
    private androidPermissions: AndroidPermissions,
    private transfer: FileTransfer,
    private file: File,
    private platform: Platform,
    private photoViewer: PhotoViewer
  ) { }

  ngOnInit() {
    this.memeForm = this.formBuilder.group({
      text1: new FormControl(""),
      text2: new FormControl("")
    });

    this.route.paramMap.subscribe(params => {
      this.name = params.get("name");
    });

    const body = document.getElementsByTagName("body")[0];
    body.classList.add("landing-page");
  }
  ngOnDestroy() {
    const body = document.getElementsByTagName("body")[0];
    body.classList.remove("landing-page");
  }

  checkPermissions() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {
        this.download(this.imgName, this.blobObj);
      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
    this.androidPermissions.requestPermissions(
      [
        this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
        this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
      ]
    );
  }

  download(name, blobObj) {
    // const url = imgUrl;

    const fileTransfer: FileTransferObject = this.transfer.create();
    let fileName = name;
    let that = this;
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => {

        var pathFile = "";
        var fileName = name;
        var contentFile = blobObj;

        pathFile = this.file.externalRootDirectory + "/Download/";

        if (result.hasPermission) {

          this.file.writeFile(pathFile, fileName, contentFile, { replace: true })
            .then(function (success) {
              //success
              let absPath = pathFile + "/" + fileName;
              that.photoViewer.show(absPath, fileName, { share: true });
              that.memeForm.value["text1"] = "";
              that.memeForm.value["text2"] = "";
              that.loaderStatus = false;
              that.btnStatus = false;
              alert("Yaasss!! Download complete");

            }, function (error) {
              alert("Oh Crap!! Error in Download.");
              that.loaderStatus = false;
              that.btnStatus = false;

            });
        } else {
          this.androidPermissions.requestPermissions(
            [
              this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE,
              this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE
            ]

          );
          this.download(this.imgName, this.blobObj);
        }

      },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
  }


  onSubmit() {
    this.loaderStatus = true;
    this.btnStatus = true;
    this.imgName = "";
    this.templateService
      .generateCustomMeme(
        this.name,
        this.memeForm.value["text1"],
        this.memeForm.value["text2"]
      )
      .subscribe((res: any) => {
        this.blobObj = new Blob([res], { type: 'image/jpeg' });
        // this.imgUrl = res.data.url;
        // var file = new File([res], "name");
        this.imgName = this.name + ((new Date).getTime()) + '.jpg';
        this.download(this.imgName, this.blobObj);
        // window.open(URL.createObjectURL(res));
        // this.download(this.imgName, this.imgUrl);
        // this.loaderStatus = false;
        // this.btnStatus = false;
      });
  }

  openNav() {
    if (this.menuStatus) {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("menu-elements").style.display = "none";
      this.menuStatus = !this.menuStatus;
    } else {
      document.getElementById("mySidenav").style.width = "50%";
      document.getElementById("menu-elements").style.display = "block";
      this.menuStatus = !this.menuStatus;
    }
  }
}
