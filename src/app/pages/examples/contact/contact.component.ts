import { Meme } from "src/app/Templates";
import { TemplateService } from "./../../../template.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-contact",
  templateUrl: "contact.component.html"
})
export class ContactComponent implements OnInit {
  menuStatus: Boolean = false;
  constructor() { }

  ngOnInit() {
  }

  openNav() {
    if (this.menuStatus) {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("menu-elements").style.display = "none";
      this.menuStatus = !this.menuStatus;
    } else {
      document.getElementById("mySidenav").style.width = "50%";
      document.getElementById("menu-elements").style.display = "block";
      this.menuStatus = !this.menuStatus;
    }
  }
}
