import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { IndexComponent } from "./index/index.component";
import { LandingpageComponent } from "./examples/landingpage/landingpage.component";
import { CustommemeComponent } from './examples/custommeme/custommeme.component';
import { ContactComponent } from './examples/contact/contact.component';
import { AboutComponent } from './examples/about/about.component';
import { GlobalService } from './../global.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
    Ng2SearchPipeModule
  ],
  declarations: [
    IndexComponent,
    LandingpageComponent,
    CustommemeComponent,
    ContactComponent,
    AboutComponent
  ],
  exports: [
    IndexComponent,
    LandingpageComponent
  ],
  providers: [
    AndroidPermissions,
    FileTransfer,
    File,
    AppMinimize,
    PhotoViewer,
    GlobalService
  ]
})
export class PagesModule { }
