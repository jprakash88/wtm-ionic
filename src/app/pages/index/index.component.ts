import { ActivatedRoute } from "@angular/router";
import { TemplateService } from "./../../template.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { Meme } from "src/app/Templates";
import { Router } from "@angular/router";
import { Route } from "@angular/compiler/src/core";
import { $ } from 'protractor';
import { Platform } from '@ionic/angular';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { GlobalService } from '../../global.service';

@Component({
  selector: "app-index",
  templateUrl: "index.component.html"
})
export class IndexComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;
  memes:any [];
  selectedFile: File;
  loaderStatus: Boolean = false;
  btnStatus: Boolean = false;
  menuStatus: Boolean = false;
  searchTerm: string = "";
  imgLoader: Boolean = false;

  constructor(
    private templateService: TemplateService,
    private router: Router,
    private platform: Platform,
    private appMinimize: AppMinimize,
    public global: GlobalService
  ) {

  }
  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }


  ngOnInit() {

    let body = document.getElementsByTagName("body")[0];
    body.classList.add("index-page");
    this.imgLoader = true;
    this.templateService.fetchTemplates().subscribe(temp => {
      this.memes = temp.data.memes;
      this.templateService.templates = this.memes;
      this.imgLoader = false;
    });
    this.platform.backButton.subscribe(() => {
      // code that is executed when the user pressed the back button
    })
    this.platform.backButton.unsubscribe();
  }
  ngOnDestroy() {
    let body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");
  }
  clearSearch() {
    this.searchTerm = "";
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.startUpload();
  }

  startUpload() {

    this.loaderStatus = true;
    this.btnStatus = true;
    this.templateService
      .customMemeUpload(this.selectedFile)
      .subscribe((res: any) => {
        if (res.status === "success") {
          this.loaderStatus = false;
          this.btnStatus = false;
          this.router.navigateByUrl("/custommeme/" + res.name);
        } else {
          this.loaderStatus = false;
          this.btnStatus = false;
          this.router.navigateByUrl("/custommeme/" + this.selectedFile.name.slice(0, -4));
        }
      });
  }

  openNav() {
    if (this.menuStatus) {
      document.getElementById("mySidenav").style.width = "0px";
      document.getElementById("menu-elements").style.display = "none";
      this.menuStatus = !this.menuStatus;
    } else {
      document.getElementById("mySidenav").style.width = "50%";
      document.getElementById("menu-elements").style.display = "block";
      this.menuStatus = !this.menuStatus;
    }
  }


}
