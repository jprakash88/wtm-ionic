import { Meme } from "src/app/Templates";
import { Templates, Response } from "./Templates";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TemplateService {
  templates: Meme[] = [];
  templateUrl = "https://api.imgflip.com/";

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  rapidApi = {
    headers: new HttpHeaders({
      "x-rapidapi-host": "ronreiter-meme-generator.p.rapidapi.com",
      "x-rapidapi-key": "1Nj4BZbPaJmshrR0MCr2AEP21Z7cp1XPS91jsnl6jm41miFg4c"
    })
  };

  

  constructor(private http: HttpClient) {}

  fetchTemplates(): Observable<Templates> {
    return this.http.get<Templates>(this.templateUrl + "get_memes");
  }

  generateMeme(id: string, boxes: string[]): Observable<Response> {
    console.log(boxes);
    let body = new HttpParams();
    // body = body.set("template_id", id);
    // body = body.set("username", "jp_decoded");
    // body = body.set("text0", "jp_decoded");
    // body = body.set("text1", "jp_decoded");

    // body = body.append("boxes", boxes);
    // &boxes[0][text]=heyheyhey&boxes[1][text]=leyleyley
    
    var boxesParam = "";
    for(let i=0;i<boxes.length;i++){
      console.log(boxes[i])
      if(i != boxes.length-1){
      boxesParam += "&boxes["+i+"][text]="+boxes[i]['text'] + "&";
      }else{
        boxesParam += "&boxes["+i+"][text]="+boxes[i]['text'];
      }
    }
    var param = "template_id="+id+"&username=jp_decoded&password=memester" + boxesParam;
    // body = body.set("password", "memester");
    console.log(param)
    

    return this.http.post<Response>(this.templateUrl + "caption_image", param, {
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded"
      })
    });
  }

  customMemeUpload(file: File) {
    console.log(file);

    const uploadData = new FormData();
    uploadData.append("image", file);
    uploadData.append("content-type", "application/json");

    return this.http.post(
      "https://ronreiter-meme-generator.p.rapidapi.com/images",
      uploadData,
      this.rapidApi
    );
  }

  generateCustomMeme(meme: string, text0: string, text1: string) {
    let body = new HttpParams();
    body = body.set("meme", meme);
    body = body.set("top", text0);
    body = body.set("bottom", text1);
    return this.http.get(
      "https://ronreiter-meme-generator.p.rapidapi.com/meme",
      {
        headers: new HttpHeaders({
          "x-rapidapi-host": "ronreiter-meme-generator.p.rapidapi.com",
          "x-rapidapi-key": "1Nj4BZbPaJmshrR0MCr2AEP21Z7cp1XPS91jsnl6jm41miFg4c"
        }),
        params: body,
        responseType: "blob"
      },
      
    );
  }
}
